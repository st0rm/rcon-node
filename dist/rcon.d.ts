import * as Websocket from 'ws';
import { ConnectionManager } from './modules/connection-manager';
import { TerminalManager } from './modules/terminal-manager';
import { PlayersManager } from './modules/players-manager';
import { ChatManager } from './modules/chat-manager';
export declare enum EventType {
    TerminalOutput = "terminal.output",
    TerminalExec = "terminal.exec",
    PlayerConnected = "player.connected",
    PlayerDisconnected = "player.disconnected",
    PlayersAll = "players.all"
}
export declare class RCON {
    registeredEvents: {};
    config: any;
    connectionManager: ConnectionManager;
    terminalManager: TerminalManager;
    playersManager: PlayersManager;
    chatManager: ChatManager;
    constructor(config: any);
    init(): void;
    shutdown(): void;
    on(event: EventType, callback: any): void;
    callEvent(event: EventType, ...args: any[]): any;
    handleTrustedRequest(data: any, ws: Websocket): void;
    broadcastTrusted(data: any): void;
    getServerData(): {
        title: string;
        service: string;
    };
}
