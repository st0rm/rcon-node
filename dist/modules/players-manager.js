"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rcon_1 = require("../rcon");
class PlayersManager {
    constructor(rcon) {
        this.rcon = rcon;
    }
    handleRequest(data, ws) {
        switch (data) {
            case rcon_1.EventType.PlayersAll:
                const players = this.rcon.callEvent(rcon_1.EventType.PlayersAll);
                return ws.send(JSON.stringify({ action: rcon_1.EventType.PlayersAll, data: players }));
        }
    }
    playerConneced(data) {
        this.rcon.broadcastTrusted({ action: rcon_1.EventType.PlayerConnected, data });
    }
    playerDisconneced(data) {
        this.rcon.broadcastTrusted({ action: rcon_1.EventType.PlayerDisconnected, data });
    }
}
exports.PlayersManager = PlayersManager;
//# sourceMappingURL=players-manager.js.map