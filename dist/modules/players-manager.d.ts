import * as Websocket from 'ws';
import { RCON } from '../rcon';
export declare class PlayersManager {
    private rcon;
    constructor(rcon: RCON);
    handleRequest(data: any, ws: Websocket): void;
    playerConneced(data: any): void;
    playerDisconneced(data: any): void;
}
