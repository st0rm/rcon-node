"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rcon_1 = require("../rcon");
class TerminalManager {
    constructor(rcon) {
        this.rcon = rcon;
    }
    handleRequest(data, ws) {
        switch (data) {
            case rcon_1.EventType.TerminalExec:
                this.rcon.callEvent(rcon_1.EventType.TerminalExec, data);
        }
    }
    outputAdded(data) {
        this.rcon.broadcastTrusted({ action: rcon_1.EventType.TerminalOutput, data });
    }
}
exports.TerminalManager = TerminalManager;
//# sourceMappingURL=terminal-manager.js.map