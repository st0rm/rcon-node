import * as Websocket from 'ws';
import { RCON } from '../rcon';
export declare class TerminalManager {
    private rcon;
    constructor(rcon: RCON);
    handleRequest(data: any, ws: Websocket): void;
    outputAdded(data: any): void;
}
