import * as Websocket from 'ws';
import { RCON } from 'rcon';
export declare class ChatManager {
    private rcon;
    constructor(rcon: RCON);
    handleRequest(data: any, ws: Websocket): void;
    messageAdded(data: any): void;
}
