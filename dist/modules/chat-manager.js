"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ChatManager {
    constructor(rcon) {
        this.rcon = rcon;
    }
    handleRequest(data, ws) {
        switch (data) {
            // case 'players.all':
            //     const players = this.rcon.callEvent('players.all')
            //     return ws.send(JSON.stringify({ action: 'players.all', data: players }));
        }
    }
    messageAdded(data) {
        this.rcon.broadcastTrusted({ action: 'chat.add', data });
    }
}
exports.ChatManager = ChatManager;
//# sourceMappingURL=chat-manager.js.map