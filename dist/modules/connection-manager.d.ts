import * as Websocket from 'ws';
import { RCON } from '../rcon';
export interface Connection {
    socket: Websocket;
    token: string;
    joined: Date;
}
export interface MessageData {
    action: string;
    data?: any;
}
export declare class ConnectionManager {
    private rcon;
    socketServer: Websocket.Server;
    trusted: Map<string, Websocket>;
    constructor(rcon: RCON);
    handleConnect(ws: Websocket): void;
    handleDisconnect(ws: Websocket): void;
    handleUntrustedAction(event: any): void;
    handleTrustedAction(event: any): void;
    addTrustedSocket(ws: Websocket): void;
    handleRequest(data: any, ws: Websocket): void;
}
