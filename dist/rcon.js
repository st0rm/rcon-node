"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const connection_manager_1 = require("./modules/connection-manager");
const terminal_manager_1 = require("./modules/terminal-manager");
const players_manager_1 = require("./modules/players-manager");
const chat_manager_1 = require("./modules/chat-manager");
var EventType;
(function (EventType) {
    EventType["TerminalOutput"] = "terminal.output";
    EventType["TerminalExec"] = "terminal.exec";
    EventType["PlayerConnected"] = "player.connected";
    EventType["PlayerDisconnected"] = "player.disconnected";
    EventType["PlayersAll"] = "players.all";
})(EventType = exports.EventType || (exports.EventType = {}));
class RCON {
    constructor(config) {
        this.registeredEvents = {};
        this.config = config;
        this.connectionManager = new connection_manager_1.ConnectionManager(this);
        this.terminalManager = new terminal_manager_1.TerminalManager(this);
        this.playersManager = new players_manager_1.PlayersManager(this);
        this.chatManager = new chat_manager_1.ChatManager(this);
    }
    init() {
        console.log("RCON Server initialized");
    }
    shutdown() {
        console.log("RCON Server shutting down");
    }
    on(event, callback) {
        this.registeredEvents[event] = callback;
    }
    callEvent(event, ...args) {
        return this.registeredEvents[event](args);
    }
    handleTrustedRequest(data, ws) {
        this.connectionManager.handleRequest(data, ws);
        this.terminalManager.handleRequest(data, ws);
        this.playersManager.handleRequest(data, ws);
        this.chatManager.handleRequest(data, ws);
    }
    broadcastTrusted(data) {
        const trusted = this.connectionManager.trusted.values();
        for (const ws of trusted) {
            ws.send(JSON.stringify(data));
        }
        ;
    }
    getServerData() {
        return {
            title: 'awesome Server',
            service: 'something'
        };
    }
}
exports.RCON = RCON;
//# sourceMappingURL=rcon.js.map