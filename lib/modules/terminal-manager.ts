import * as Websocket from 'ws';
import { RCON, EventType } from '../rcon';

export class TerminalManager {

    constructor(private rcon: RCON) { }

    handleRequest(data, ws: Websocket) {
        switch (data) {
            case EventType.TerminalExec:
                this.rcon.callEvent(EventType.TerminalExec, data)
        }
    }

    outputAdded(data) {
        this.rcon.broadcastTrusted({ action: EventType.TerminalOutput, data })
    }

}
