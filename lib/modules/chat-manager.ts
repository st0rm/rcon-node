import * as Websocket from 'ws';
import { RCON } from 'rcon';

export class ChatManager {

    constructor(private rcon: RCON) {

    }

    handleRequest(data, ws: Websocket) {
        switch (data) {
            // case 'players.all':
            //     const players = this.rcon.callEvent('players.all')
            //     return ws.send(JSON.stringify({ action: 'players.all', data: players }));
        }
    }

    messageAdded(data) {
        this.rcon.broadcastTrusted({ action: 'chat.add', data })
    }

}
