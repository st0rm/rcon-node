
import * as Websocket from 'ws';
import { v4 as uuid } from 'uuid';

import { RCON } from '../rcon';

export interface Connection {
    socket: Websocket,
    token: string,
    joined: Date,
}

export interface MessageData {
    action: string,
    data?: any
}

export class ConnectionManager {

    socketServer: Websocket.Server;

    trusted: Map<string, Websocket> = new Map();

    constructor(private rcon: RCON) {
        this.socketServer = new Websocket.Server({ port: 8080 });

        this.socketServer.on('connection', (ws) => this.handleConnect(ws));
        this.socketServer.on('error', console.log);
    }

    handleConnect(ws: Websocket) {
        console.log('socket connected');
        ws.onmessage = (event) => this.handleUntrustedAction(event);
        ws.onclose = (event) => this.handleDisconnect(event.target);
        ws.onerror = (event) => this.handleDisconnect(event.target);
    }

    handleDisconnect(ws: Websocket) {
        console.log('socket count: ' + this.trusted.size)
        console.log('socket disconnected')
        this.trusted.forEach((value, key, map) => {
            if (value === ws) {
                map.delete(key)
            }
        });
        console.log('socket count: ' + this.trusted.size)
    }

    handleUntrustedAction(event) {
        if (event.type !== 'message') {
            console.warn(`message with type: ${event.type} given`)
            return;
        }
        const message = JSON.parse(event.data as string) as MessageData;
        switch (message.action) {
            case 'login':
                if (message.data.user === 'admin', message.data.password === 'test') {
                    return this.addTrustedSocket(event.target)
                }
                event.target.terminate()
                break;

            default:
                console.log('unknown message', message)
        }
    }

    handleTrustedAction(event) {
        const message = JSON.parse(event.data as string) as MessageData;
        switch (message.action) {
            case 'logout':
                return this.handleDisconnect(event.target)

            case 'request':
                return this.rcon.handleTrustedRequest(message.data, event.target)

            default:
                console.log('unknown message', message)
        }
    }

    addTrustedSocket(ws: Websocket) {
        ws.onmessage = (event) => this.handleTrustedAction(event);

        const token = uuid()
        this.trusted.set(token, ws)

        ws.send(JSON.stringify({ action: 'authenticated', data: token }))
        ws.send(JSON.stringify({ action: 'info', data: this.rcon.getServerData() }))

        console.log('socket count: ' + this.trusted.size)
    }

    handleRequest(data, ws: Websocket) {
        switch (data) {
            case 'info':
                return ws.send(JSON.stringify({ action: 'info', data: this.rcon.getServerData() }));
        }
    }

}
