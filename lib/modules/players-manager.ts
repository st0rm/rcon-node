import * as Websocket from 'ws';
import { RCON, EventType } from '../rcon';

export class PlayersManager {

    constructor(private rcon: RCON) {

    }

    handleRequest(data, ws: Websocket) {
        switch (data) {
            case EventType.PlayersAll:
                const players = this.rcon.callEvent(EventType.PlayersAll)
                return ws.send(JSON.stringify({ action: EventType.PlayersAll, data: players }));
        }
    }

    playerConneced(data) {
        this.rcon.broadcastTrusted({ action: EventType.PlayerConnected, data })
    }

    playerDisconneced(data) {
        this.rcon.broadcastTrusted({ action: EventType.PlayerDisconnected, data })
    }

}
