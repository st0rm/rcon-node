import * as Websocket from 'ws';

import { ConnectionManager } from './modules/connection-manager';
import { TerminalManager } from './modules/terminal-manager';
import { PlayersManager } from './modules/players-manager';
import { ChatManager } from './modules/chat-manager';

export enum EventType {
    TerminalOutput = 'terminal.output',
    TerminalExec = 'terminal.exec',

    PlayerConnected = 'player.connected',
    PlayerDisconnected = 'player.disconnected',
    PlayersAll = 'players.all',
}

export class RCON {

    registeredEvents = {}

    config: any
    connectionManager: ConnectionManager
    terminalManager: TerminalManager
    playersManager: PlayersManager
    chatManager: ChatManager

    constructor(config) {
        this.config = config;

        this.connectionManager = new ConnectionManager(this)
        this.terminalManager = new TerminalManager(this)
        this.playersManager = new PlayersManager(this)
        this.chatManager = new ChatManager(this)
    }

    init() {
        console.log("RCON Server initialized")
    }

    shutdown() {
        console.log("RCON Server shutting down")
    }

    on(event: EventType, callback) {
        this.registeredEvents[event] = callback
    }

    callEvent(event: EventType, ...args) {
        return this.registeredEvents[event](args);
    }

    handleTrustedRequest(data, ws: Websocket) {
        this.connectionManager.handleRequest(data, ws)
        this.terminalManager.handleRequest(data, ws)
        this.playersManager.handleRequest(data, ws)
        this.chatManager.handleRequest(data, ws)
    }

    broadcastTrusted(data) {
        const trusted = this.connectionManager.trusted.values()
        for (const ws of trusted) {
            ws.send(JSON.stringify(data))
        };
    }

    getServerData() {
        return {
            title: 'awesome Server',
            service: 'something'
        }
    }
}
