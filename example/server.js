const {
    RCON,
    EventType
} = require('../dist/index')

class Game {

    constructor() {
        this.terminal = []
        this.players = []
        this.chat = []

        this.init()
        this.main()
    }

    init() {
        this.rcon = new RCON({
            user: 'admin',
            password: 'test'
        })

        this.rcon.on(EventType.PlayersAll, () => {
            return this.players
        })
        this.rcon.on('player.single', (id) => {
            return this.players.find(player => player.id === id)
        })
        this.rcon.on('player.kick', (id) => {
            return this.kickPlayer(id)
        })
        this.rcon.on('chat', (message) => {
            return this.addMessage(-1, message)
        })

        this.rcon.init()
    }

    addPlayer(player) {
        this.addTerminalOutput(`${player.name} connected`)
        this.players.push(player)
        this.rcon.playersManager.playerConneced(player)
    }

    removePlyer(id) {
        const index = this.players.findIndex(player => player.id === id)
        const player = this.players[index]
        this.rcon.playersManager.playerDisconneced(player)
        this.players = this.players.splice(index)
        this.addTerminalOutput(`${player.name} disconnected`)
    }

    kickPlayer(id) {
        this.addTerminalOutput(`player kicked: ${ id }`)
        this.addChat(-1, `player kicked: ${ id }`)
        this.removePlyer(id)
    }

    addChat(playerId, text) {
        const message = {
            time: Date.now(),
            playerId,
            text
        }
        this.chat.push(message)
        this.rcon.chatManager.messageAdded(message)
    }

    addTerminalOutput(text) {
        this.terminal.push(text)
        this.rcon.terminalManager.outputAdded(text)
    }

    shutdown() {
        this.rcon.shutdown()
        clearInterval(this.gameLoop)
    }

    main() {
        let counter = 0
        this.gameLoop = setInterval(() => {

            if (randomChance(50)) {
                this.addPlayer({
                    name: `player ${counter++}`,
                    id: uuid()
                })
            }

            if (this.players.length < 1) return

            if (randomChance(50)) {
                this.addChat(this.findRandomPlayer().id, 'bla')
            }

            if (randomChance(10)) {
                this.removePlyer(this.findRandomPlayer().id)
            }

        }, 1000)
    }

    findRandomPlayer() {
        return this.players[getRandomInt(0, this.players.length - 1)]
    }
}

function randomChance(percent) {
    return Math.random() <= (percent / 100)
}

function getRandomInt(min, max) {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min
}

function uuid() {
    return 'xxxx-xxxx-xxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8)
        return v.toString(16)
    })
}

const game = new Game()
